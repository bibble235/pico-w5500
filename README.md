# pico-w5500

This project demonstrate how to connect a Pico RP2040 with a W5500. 

It provides demonstration of a web server
## Pre-requisites
VS Code
Platform IO Plugin

## Installation

Clone the project with.

```bash
git clone https://gitlab.com/bibble235/pico-w5500
```
## Usage

Build the project  
Change the platformio.ini upload_port to be your location  
Upload  

# Connecting the Pico
VCC  
GND  
MOSI: 19  
MISO: 16  
SCK: 18  
SS: 17  
## Contributing

Thanks to https://github.com/khoih-prog
## License

[MIT](https://choosealicense.com/licenses/mit/)